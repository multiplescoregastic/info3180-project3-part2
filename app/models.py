from app import db

class Profile(db.Model):
  userid = db.Column(db.Integer, primary_key=True, nullable=False, unique=True)
  fname = db.Column(db.String(80),nullable=False, index=True)
  lname = db.Column(db.String(80), nullable=False, index=True)
  username = db.Column(db.String(80), index=True, nullable=False, unique=True)
  email = db.Column(db.String(80), index=True, nullable=False, unique=True)
  password = db.Column(db.String(80), index=True, nullable=False)
  sex = db.Column(db.String(10), nullable=False, index=True)
  age = db.Column(db.String(25), nullable=False, index=True)
  highscore = db.Column(db.Integer)
  tdollar = db.Column(db.Integer)
  profile_add_on = db.Column(db.String(80), nullable=False)
  image = db.Column(db.String(80), nullable=False) 
  #confirmation_id = db.Column(db.String(80), index=True, nullable=False, unique=True)
  #confirmation_status = db.Column(db.String(5), index=True, nullable=False)

  def is_authenticated(self):
    return True

  def is_active(self):
    return True

  def is_anonymous(self):
    return False

  def get_id(self):
    try:
      return unicode(self.userid)
    except NameError:
      return str(self.userid)
  
  def __repr__(self):
    return "<User %r>" % (self.username)
